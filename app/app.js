var app = angular.module('RepoSearchApp', ['ngRoute', 'chart.js']);

app.config(function ($routeProvider) { 
  $routeProvider 
    .when('/', { 
      controller: 'RepoController', 
      templateUrl: 'app/views/repos.html' 
    })
    .when('/issues/:user/:repo', {
      controller: 'IssueController', 
      templateUrl: 'app/views/issues.html' 
    })
    .otherwise({ 
      redirectTo: '/' 
    }); 
});