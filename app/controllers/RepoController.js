app.controller('RepoController', function($rootScope, $scope, $location, repos) {
  $scope.tags = [
    "bootstrap",
    "jquery",
    "angularjs"
  ];
  $scope.loading = false;

  $scope.searchRepo = () => {
    $scope.loading = true;
    repos.getRepositories($scope.searchText).then(
      (data) => {
        $scope.repos = data;
      }).finally(() => {
        $scope.loading = false;
      });
  };

  $scope.tagClicked = (tag) => {
    $scope.searchText = tag
    $scope.searchRepo()
  };

  $scope.expandSelected = repo => {
    $scope.repos.items.forEach(repo => {
      repo.expanded = false;
    })

    $scope.data = [
      repo.forks_count,
      repo.open_issues_count
    ];

    $scope.labels = ["Forks", "Open issues"];
    repo.expanded = true;
  }

  $scope.hideSelected = repo => {
    repo.expanded = false;
  }
});