angular.module( 'TableController', function($scope) {
  $scope.expandSelected = repo => {
    $scope.repos.items.forEach(repo => {
      repo.expanded = false;
    })

    $scope.data = [
      repo.forks_count,
      repo.open_issues_count
    ];

    $scope.labels = ["Forks", "Open issues"];
    repo.expanded = true;
  }

  $scope.hideSelected = repo => {
    repo.expanded = false;
  }
})