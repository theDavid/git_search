app.controller('IssueController', function($scope, issues, $routeParams) {
  $scope.repo = $routeParams.user + "/" + $routeParams.repo
  $scope.loading = true;
  issues.getIssues($scope.repo)
  .then(data => {
    $scope.issues = data;
  })
  .finally(() => {
    $scope.loading = false;
  });
});
