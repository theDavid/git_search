app.factory('issues', function($q, $http) {
  const getIssues = (keyword) => {
    var deferred = $q.defer();

    $http.get('https://api.github.com/search/issues?q=' + keyword)
      .then((res) => {
        deferred.resolve( res.data );
      });

    return deferred.promise;
  }

  return {
    getIssues: getIssues,
  }
});
