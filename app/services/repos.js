app.factory('repos', function($q, $http) {
  const getRepositories = (keyword) => {
    var deferred = $q.defer();

    $http.get('https://api.github.com/search/repositories?q=' + keyword)
      .then((res) => {
        deferred.resolve( res.data );
      });

    return deferred.promise;
  }

  return {
    getRepositories: getRepositories,
  }
});